function ExpanderCtrl($scope) {
    $scope.companies = [company("comp 1", false), company("comp 2", true)];


    $scope.showChildrenFor = function (company) {
        if (company.loaded != true) {
            loadChildrenFor(company);
        }
        company.expanded = !company.expanded;
    };

    function company(name, group) {
        return {name: name, group: group, children: [], expanded: false};
    }

    function loadChildrenFor(parent) {
        parent.loaded = true;
        var number = Math.floor(Math.random() * 10) + 1;
        for (var i = 0; i < number; i++) {
            parent.children.push(company(parent.name + "-" + i, true));
        }

    }
}