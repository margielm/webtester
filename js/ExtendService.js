var module = angular.module('app', []);
module.config(function ($httpProvider) {
    $httpProvider.responseInterceptors.push(interceptor);
});
var interceptor = function ($q) {
    return function (promise) {

        return promise.then(
            function (response) {
                var firstChar = response.data.charAt(0);
                if (firstChar != '[' && firstChar != '{') {
                    $q.reject(response);
                } else {
                    return response;
                }
            }, function (response) {
                console.log("-");
                return $q.reject(response);
            });
    }
};
function TestCtrl($scope, $http) {
    $scope.click = function () {
        console.log("click");
        $http.get("http://jsfiddle.net/echo/json", {params: {}})
            .success(function (data) {
                console.log(data);
            }).error(function () {
                console.log("error");
            });
    };
}